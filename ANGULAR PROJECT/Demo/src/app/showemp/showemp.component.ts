import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemp',
  templateUrl: './showemp.component.html',
  styleUrls: ['./showemp.component.css']
})
export class ShowempComponent implements OnInit {
  employees:any;

  constructor(private httpClient: HttpClient,private service: EmpService) {
   // this.employees = [{ empId: 1000, empName: 'ELIAS', salary: 9999.99,gender:'M',doj:'04-09-1999'},
    // { empId: 2000, empName: 'PASHA', salary: 8888.88,gender:'M',doj:'12-12-2018'},
    // { empId: 3000, empName: 'JOHN', salary: 7777.77,gender:'M',doj:'09-10-2004'},
    // { empId: 4000, empName: 'LUCI', salary: 7887.76,gender:'F',doj:'10-11-2012'}];
  }

  ngOnInit(): void {
    this.service.fetchDetails().subscribe((result:any)=>{
      this.employees = result;
    });
  }

  deleteEmp(employee:any){
    this.service.deleteEmp(employee).subscribe((result:any)=> {
      console.log(result);
      const i = this.employees.findIndex((element:any)=>{
        return element.email === employee.email;
      })
      this.employees.splice(i,1);
    });
  }
}

