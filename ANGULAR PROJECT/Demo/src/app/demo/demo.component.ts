import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  message: string;
  age : number;
  name: string;
  salary : number;
  isMarried: boolean;
  hobbies : any; //it can be number,string,boolean,object
  address : any;

  constructor() {
    this.message = 'Hello World';
    this.age = 33;
    this.name = 'HARSHA';
    this.salary = 9999.99;
    this.isMarried = false;
    this.hobbies = ['PLAYING','CHATTING','EATING','SLEEPING','READING','EXERCISE'];
    this.address = {doorNo:101 , street:'GACHIBOWLI', city:'HYDERABAD'};
  }

  ngOnInit(): void { 
  }
  showMessage() : void{
    alert('Method called...');
    console.log('method called...');
  }
}
