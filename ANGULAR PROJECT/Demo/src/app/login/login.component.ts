import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginId : string;
  password : string;
  employee: any;

  constructor(private router: Router,private service: EmpService, private httpClient:HttpClient) { //Dependency Injection
    this.loginId ='';
    this.password = '';
    // this.employee = [{id:1,name:'PASHA',email:'pasha123@gmail.com',password:'password'},
    // {id:1,name:'HARSHA',email:'harsha123@gmail.com',password:'password'},
    // {id:1,name:'RAHUL',email:'rahul123@gmail.com',password:'password'}];
   }

  ngOnInit(): void {
  }

  loginSubmit() :void {
    if(this.loginId === 'HR' && this.password === 'HR'){
      this.service.setUserLoggedIn();
      this.router.navigate(['hrhomepage']);
    } else {
      this.employee.forEach((element:any) => {
        if(element.email === this.loginId){
          this.service.setUserLoggedIn();
          alert('Welcome to Employee Home Page..')
        }
      });
    }
  }

async loginSubmit2(loginForm : any) {
 if(loginForm.email === 'HR' && loginForm.password === 'HR'){
  this.service.setUserLoggedIn();
  this.router.navigate(['hrhomepage']);
} else {
   await this.service.fetchDetailsByEmailAndPassword(loginForm).then((result:any)=>{
     this.employee = result;
   });
   console.log('Recieved Detils',this.employee);
   if(this.employee!=null)
   alert('Welcome to Employee Home Page');
  this.service.setUserLoggedIn();
    }
  }
}

