import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  employee: any;
  constructor(private httpClient: HttpClient,private service:EmpService) {
    this.employee ={empName:'',gender:'',joinDate:'',salary:'',email:'',password:''};
   }

  ngOnInit(): void {
    //this.httpClient.get('http://restcountries.com/v3/all').subscribe((result:any) =>
    //console.log(result));
    console.log('Data Recieved ...');//OUT OF SUBSCRIPTION
  }
  registerEmp(){
    this.service.registerEmp(this.employee).subscribe(
      (result:any)=>console.log(result));
  }
}

