import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  employees:any;
  constructor() { }

  ngOnInit(): void {
    this.employees = [{empId: 100, empName: 'PASHA',salary: 9999.99},
    {empId: 101, empName: 'HARSHA',salary: 988988.88},
    {empId: 102, empName: 'SANJANA',salary: 787877.77},
    {empId: 103, empName: 'VENKAT',salary: 677677.77},
    {empId: 104, empName: 'INDIRA',salary: 988999.99}];

  }
  showDetails(data:any){
    alert(data);
  }

}
