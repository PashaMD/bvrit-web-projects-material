import { Component, OnInit } from '@angular/core';
import { EmpService } from './../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  countries: any;
  constructor(private service: EmpService) {
  }

  ngOnInit() {
    this.service.getCountriesList().subscribe((data: any) => {console.log(data); this.countries = data; });
  }
  register(registerForm: any): void {
    this.service.registerEmp(registerForm).subscribe((result: any) => { console.log(result); } );
    console.log(registerForm);
  }
}
